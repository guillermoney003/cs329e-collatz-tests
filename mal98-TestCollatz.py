#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "150 465\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 150)
        self.assertEqual(j, 465)

    def test_read_3(self):
        s = "300 270\n"
        i, j = collatz_read(s)
        self.assertEqual(i,300)
        self.assertEqual(j, 270)

    def test_read_4(self):
        s = "150 465\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 150)
        self.assertEqual(j, 465)    

    # ----
    # eval
    # ----

    def test_eval_0(self):
        v = collatz_eval(15500, 30)
        self.assertEqual(v, 276)

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(7999, 8000)
        self.assertEqual(v, 190)

    def test_eval_5(self):
        v = collatz_eval(999999, 1)
        self.assertEqual(v, 525)

    def test_eval_6(self):
        v = collatz_eval(935, 935)
        self.assertEqual(v, 130)


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 999999, 1, 525)
        self.assertEqual(w.getvalue(), "999999 1 525\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 1500, 140, 182)
        self.assertEqual(w.getvalue(), "1500 140 182\n")        
        
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("1500 140\n999999 1\n935 935\n999999 999999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "1500 140 182\n999999 1 525\n935 935 130\n999999 999999 259\n")

    def test_solve_3(self):
        r = StringIO("999 10\n2000 3000\n520 1620\n1345 2\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "999 10 179\n2000 3000 217\n520 1620 182\n1345 2 182\n")

    def test_solve_4(self):
        r = StringIO("1000 1000\n1234 123\n55555 44444\n1 999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "1000 1000 112\n1234 123 182\n55555 44444 340\n1 999 179\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
